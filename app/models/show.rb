class Show
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :screen_play, type: String

  embeds_many :show_artists
  embeds_many :events
end
