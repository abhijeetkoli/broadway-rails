class Event
  include Mongoid::Document
  include Mongoid::Timestamps

  field :date, type: Date
  field :time, type: Time
  field :price, type: BigDecimal
  
  embedded_in :theater
  embedded_in :show
end
