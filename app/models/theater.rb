class Theater
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :name, type: String
  field :location_lat, type: BigDecimal
  field :location_lang, type: BigDecimal
  field :address, type: String

  embeds_many :events
end
