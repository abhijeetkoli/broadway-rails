class Artist
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :first_name, type: String
  field :middle_name, type: String
  field :last_name, type: String

  embeds_many :show_artists
end
