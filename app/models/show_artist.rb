class ShowArtist
  include Mongoid::Document
  include Mongoid::Timestamps
  
  embedded_in :show
  embedded_in :artist
end
